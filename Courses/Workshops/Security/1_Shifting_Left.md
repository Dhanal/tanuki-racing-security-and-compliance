### Create a merge request to add security scans to our pipeline

### Theme

This section focuses on shifting left as a security practice and how your code changes will display security results after a commit rather than months down the line

# Step 1: Adding Security Scans

1. First make sure that you are on the main page of the project you just forked in. It is best if you have these instructions open in a separate tab/screen while completing the tasks.
  
2. Once ready use the left hand navigation menu to click through **Build \> Pipeline editor**. Here you will see the current set up of our main branch pipeline. Notice that there are two stages, which are further defined below.
  
3. This pipeline does very little in terms of security scanning and only has a simple unit test defined currently. Lets go ahead and create a new branch to add our changes. Use the left hand navigation menu to click through **Code \> Branches** then click **New branch**. Name the branch **_secure-pipeline_** and make sure it is based off of **_main_**, then click **Create Branch**.
  
4. Once again use the left hand navigation menu to click through **Build \> Pipeline editor** to get back to the editor page. Make sure that the branch dropdown in the top left of the editor view shows **_secure-pipeline_**, or select the branch if it doesn't. We then want to change the pipeline yaml to be the code below:

```plaintext
image: docker:latest

services:
  - docker:dind

variables:
  CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
  DOCKER_DRIVER: overlay2
  ROLLOUT_RESOURCE_TYPE: deployment
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
  DAST_BAS_DISABLED: "true"
  CI_DEBUG_TRACE: "true"
  # CI_PROJECT_PATH_SLUG: "tanukiracing"
  SECRET_DETECTION_EXCLUDED_PATHS: "Courses, Golden_Demos"
  

stages:
  - build
  - test
  - dast

include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST-IaC.gitlab-ci.yml
  - template: DAST.gitlab-ci.yml

build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE

sast:
  variables:
      SEARCH_MAX_DEPTH: 12

dast:
  services: # use services to link your app container to the dast job
    - name: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
      alias: app
  variables:
    DAST_TARGET_URL: http://app:8080
    DAST_BROWSER_SCAN: "true" # use the browser-based GitLab DAST 
    REVIEW_DISABLED: 'false' # ensure that DAST can run on feature branches as well for demo purposes
    DAST_CRAWL_GRAPH: "true"
    DAST_VERSION: "4" # pin to the previous version to allow for a stable state of detection rules
  artifacts:
    paths: [gl-dast-crawl-graph.svg, gl-dast-report.json]
    when: always
```

5. First looking at the **_include_** section you can see that a number of security templates have been brought into our project. These define different scans and jobs that will now be ran based off of our **_stages_**. To get a better look into the templates you can click **Full configuration** which will show the true pipeline yaml with all of the templates brought in. You can also click the branch icon in the top left to then click into a specific template to get its definition.

6. Click on the **Edit** tab again to be brought back to our normal editor. Notice that at the end of the file we have defined two additional jobs. 
    - First we are overriding `SEARCH_MAX_DEPTH` in our SAST job included from the **_Jobs/SAST.gitlab-ci.yml_** template to specify the depth at which our tooling will detect the programming languages being used to select the appropriate analyzers. 
    - Secondly we are overriding the DAST job included from the **_DAST.gitlab-ci.yml_** template to [use our Docker image that we are building as a service](https://docs.gitlab.com/ee/user/application_security/dast/#docker-services) that will be analyzed by our DAST scanner in our pipeline. With the additional CI/CD variables, we are configuring the DAST scanner use the browser based scanner for modern web applications, enable feature branches to run the DAST scan and enable the DAST crawl graph artifact to show a map of what pages were crawled. 

7. Now that our changes are in lets click **Commit changes** at the bottom of the page.

> If you run into any issues you can use the left hand navigation menu to click through **CI/CD -\> Pipelines**, click **Run pipeline**, select **_security-workshop-pipeline_** and click **Run pipeline** once again.

> [Docs for GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
  
#  Step 2: Creating the Merge Request

1. Now to actually merge in the new pipeline we want to use the left hand navigation menu to click through **Code \> Branches** & then click **Merge request** in the secure-pipeline section. **DOUBLE CHECK THAT THE FORK RELATIONSHIP WAS REMOVED BEFORE DOING THIS**
  
2. On the resulting page scroll down to the **_Merge options_** and uncheck **Delete source branch when merge request is accepted**. You can leave the rest of the settings as is then click **Create merge request**.
  
3. First resolve any merge conflicts that may exist, but you should see a pipeline kick off. If you click the hyperlink it will bring you to the pipeline view where you can see all of the various jobs that we added to our yaml file. While this runs we are going to move forward but we will check back in a bit to see the results of our scanners.

