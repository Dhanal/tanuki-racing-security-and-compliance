### In this section, we will introduce a preventative security policy for our project.

# Step 1: Preventive Security Policies

1. We next want to navigate back to the _Vulnerability report_ and try to change **Tool** to _Secret Detection_. Notice that Secret Detection is not listed! This means we haven't leaked any secure tokens so far. We surely want to keep it that way - it happens far too often.
  
2. To prevent this from ever happening in the future we can set up a new policy to run on all future merge requests. For our use case leaked tokens are easy mistakes that can lead to massive problems so we will create a quick policy to stop that. Use the left hand navigation menu to click through **Secure \> Policies** and then click **New policy**. On the resulting page click **Select policy** under **_Merge Request Approval Policy_**.
  
3. Add a **name** to the policy, then under the **_Rules_** section we want to select **Security Scan** in the **When** dropdown list. Then we want to change **All scanners** to be **_Secret Detection_** and **All protected branches** to **default branch**.
  
4. Then under actions choose **individual users** as the **_Choose approver type_** and add **_lfstucker_** as the required approver. Next uncheck **Prevent pushing and force pushing**.

> Please ensure you do this step or you will be blocked later on in the workshop

5. Lastly click **Configure with a merge request**. On the resulting merge request click ***merge*** and you will be brought to your new policy project that is applied to our workshop application. If you were to create another merge request with the leaked token still in the code base, merging would be prevented until it was removed or you added your approval.
  
6. Lastly use the breadcrumbs at the top of the screen to click into your group, then once again click into your project.

> [Docs for policies](https://docs.gitlab.com/ee/user/application_security/policies/)

# Step 2: Scan Execution Policy

1. Our new secret detection policy will now be enforced with every commit. However approval mistakes happen, so we are going to set an additional scan execution policy to check for secrets that were mistakenly approved daily. 

2. Once again use the left hand navigation menu to click through **Secure > Policies**, then click **New policy**. On the resulting page click **Select policy** under **_Scan execution policy_**.

3. Add a **name** to the policy, description, then under the **_Actions_** section we want to select _Run a_ **Secret Dectection**. Then we want to scroll to the _Conditions_ section and change _Triggers:_ to _Schedules:_, change _specific protected branches_ to **default** branch, then select your timezone.

5. Lastly click **Configure with a merge request**. On the resulting merge request click ***merge*** and you will be brought to your new policy project that is applied to our workshop application. Now this scan will run daily and alert us if a secret was accidentally approved into our default branch.
  
6. Lastly use the breadcrumbs at the top of the screen to click into your group, then once again click into your project.

# Step 3: Secret Detection In Action

1. Now we want to see our merge request approval policy working in action for secrets detection. From the main page of our project lets go ahead and click **Web IDE** in the **Edit** dropdown list.
  
2. Click into the **_cf-sample-scripts/eks.yaml_** file and add a fake AWS token at the end of the line 6. Change the **description** from **_The name of the IAM role for the EKS service to assume._** to **The name of the IAM role for the EKS service to assume, using aws_key_id AKIAIOSF0DNN7EXAMPLE.**.
  
3. Once added click the source control button on the left hand side, add a quick commit message, then click the **down arrow**.
  
4. On the resulting drop down click **Yes** to open a new branch, then click the **_Enter_** key. A new popup will appear where we want to then click **Create MR**

5. Scroll to the bottom, uncheck **_Delete source branch when merge request is accepted_**, and click **Create merge request**
  
6. On the resulting MR notice that our policy requires approval from **_lfstucker_** and is blocked by our two policies before we are able to merge. Wait for the entire pipeline to finish running.

7. When the pipeline is done running you can see that our Merge request approval policy have been enacted restricting us from committing this code. If we did not add a secret token directly to our source code we would be able to merge our code. (you can come back to this merge request to review the blocked merge after the next section)

> [Docs on automatically revoking secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#responding-to-a-leaked-secret)
